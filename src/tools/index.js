import { CONFIG, APIPath } from '../config/index'
import fetch from 'isomorphic-fetch'

export function getComponentWithType(type, componentsList) {
    return componentsList[type]
}

export function getComponents(data, componentsList) {
    const components = data.map((component, index) => {
        let Component = getComponentWithType(component.bloc_type, componentsList)
        return Component?<Component data={component[component.bloc_type]} key={index}/>:null
    })

    return components
}

function queryToAPIPath(query) {
    return APIPath[query]
}

export const callAPI = async (slug, url = CONFIG.url) => {

    queryToAPIPath(slug)
    // const res = await fetch(url + queryToAPIPath(slug))
    const res = await fetch(url + queryToAPIPath(slug))
    .then((response) => {
        return response.json();
    })
    .catch((error) => {
        console.warn(error)
    })
        
    return res
}


// WebGL
export function lerp(current, target, speed = 0.1, limit = 0.001) {
    let change = (target - current) * speed;
    if (Math.abs(change) < limit) {
      change = target - current;
    }
    return change;
  }
  