import HomepageHeader from './homepageheader'
import AboutMe from './aboutMe'
import SelectedWorks from './selectedWorks'
import Contact from './contact'

export const blocks = {
    homepageheader: HomepageHeader,
    about_me : AboutMe,
    selected_works : SelectedWorks,
    contact : Contact
}

export function getBlockWithType(type) {
    return blocks[type]
}
