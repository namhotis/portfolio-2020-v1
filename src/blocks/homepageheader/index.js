import css from './styles.scss'
import disableScroll from 'disable-scroll';

export default class HomepageHeader extends React.Component {
    constructor(props) {
        super(props)
        this.bounced = React.createRef()
        this.firstPanelScroll = React.createRef()
        this.secondPanelScroll = React.createRef()
        this.name = React.createRef()
        this.job = React.createRef()
        this.nameDiv = React.createRef()

        this.handleScroll = this.handleScroll.bind(this)
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll)

        this.handleScroll()
    }

    handleScroll(e) {
        let currentScroll = window.pageYOffset || 0
        
        if (this.job && this.job.current) {
            this.job.current.style.marginLeft = -currentScroll + 400 + 2200 + "px"
        }

        if (this.firstPanelScroll.current && 100 - currentScroll/10 > 0) {
            this.name.current.style.marginTop = 0;
            this.firstPanelScroll.current.style.width = currentScroll/10 + "%";
        } else if (this.firstPanelScroll.current) {
            this.name.current.style.marginTop = -(100 - currentScroll/10)*10+"px";
            this.firstPanelScroll.current.style.width = "100%";
        }

        if (this.secondPanelScroll.current && currentScroll > 1000 && currentScroll < 2600) {
            this.secondPanelScroll.current.style.clipPath = `polygon(0% ${100 - (currentScroll - 1000)/10}%, 100% ${100 - (currentScroll - 1000)/10}%, 100% 100%, 0% 100%)`;
        } else if (this.secondPanelScroll.current && currentScroll > 2600) {
            this.secondPanelScroll.current.style.clipPath = `polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)`;
        } else if (this.secondPanelScroll.current) {
            this.secondPanelScroll.current.style.clipPath = `polygon(0% 100%, 100% 100%, 100% 100%, 0% 100%)`;
        }

        if (this.job.current && currentScroll > 1600) {
            this.job.current.style.marginLeft = -currentScroll + 400 + 1600 + "px"
        } 
    }

    render() {
        return (
            <section id="homepageHeader" className={css.homepageHeader}>
                <ul className={css.socialMedias}>
                    <li key="Instagram">
                        <a href="https://www.instagram.com/nopain_nogluten/">
                            <img src="https://mathis-dyk.fr/backend/wp-content/uploads/2020/01/instagram-2.png" />
                        </a>
                    </li>
                    <li key="Twitter">
                        <a href="https://twitter.com/MDBdev">
                            <img src="https://mathis-dyk.fr/backend/wp-content/uploads/2020/01/twitter-3.png" />
                        </a>
                    </li>
                    <li key="Linedin">
                        <a href="https://www.linkedin.com/in/mathis-dyk-3966ab147/">
                            <img src="https://mathis-dyk.fr/backend/wp-content/uploads/2020/01/linkedin-2.png" />
                        </a>
                    </li>
                </ul>
                <span className={css.textIntro}>MATHIS&nbsp;DYK</span>
                <div ref={this.firstPanelScroll} className={css.firstPanelScroll}>
                    <div ref={this.nameDiv} className={css.nameDiv}><h1 ref={this.name}>Mathis&nbsp;DYK</h1></div>
                </div>
                <div ref={this.secondPanelScroll} className={css.secondPanelScroll}>
                    <h2 ref={this.job}>Front&nbsp;End&nbsp;Developer</h2>
                </div>
                <div className={css.scrollDiv}>
                    <a href="#aboutMe" ref={this.bounced}>Scroll</a>
                    <div className={css.whiteBar}></div>
                </div>
            </section>
        )
    }
}