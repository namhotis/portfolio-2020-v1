import css from './styles.scss'
import { getComponents } from '../../tools/index.js'
import { COMPONENTS } from '../../components/index.js'

export default function AboutMe(props) {
    let components = getComponents(props.data, COMPONENTS)

    return (
        <section id="aboutMe" className={css.aboutMe} id="aboutMe">
            <div className={css.insideAboutMe}>
                {components}
            </div>
        </section>
    )
}