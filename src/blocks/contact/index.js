import css from './styles.scss'
import { getComponents } from '../../tools/index.js'
import { COMPONENTS } from '../../components/index.js'

export default function Contact(props) {
    let components = getComponents(props.data, COMPONENTS)

    return (
        <section className={css.contact} id="contact">
            <div className={css.insideContact}>
                {components}

                <div className={css.ml}>Copyright 2020 - Mathis DYK</div>
            </div>
        </section>
    )
}