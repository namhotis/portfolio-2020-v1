export const vertexShader = `
    varying vec3 vNormal;
    varying vec2 vUv;
    uniform float uTime;
    void main(){
        vec3 pos = position.xyz;
        float time = sin(uTime) * 0.5 +0.5;
        float normalZ = pos.z / 10.;
        pos.z += smoothstep(normalZ*0.5,1., time ) * -50.;
        gl_Position = projectionMatrix * modelViewMatrix * vec4(pos,1.);
        vNormal = normal;
        vUv = uv;
    }
`;
export const projectUniforms = `
    #define USE_FOG;
    uniform float uTime;
    uniform float uStartTime;
    uniform float uDuration;
    uniform float uOffset;
    varying vec2 vUv;
`;
export const projectVertex = `
    float duration = uDuration;
    float elapsed = clamp(uTime - uStartTime,0.,duration) / duration;
    float time = sin(elapsed * PI * 2. + PI/2.) * 0.5 +0.5;
    time = abs(elapsed-0.5)*2. ;
    float normalZ = transformed.z / 10.;
    transformed.z += smoothstep(1.,normalZ*0.5, time ) * -uOffset;
    // transformed.z += 50.;
    vec4 mvPosition = modelViewMatrix * vec4( transformed, 1.0 );
    gl_Position = projectionMatrix * mvPosition;
    vUv = uv;
`;
export const fragmentShader = `
    varying vec3 vNormal;
    varying vec2 vUv;

    void main(){
        vec3 color = vec3(1.);
        // color = vNormal;
        color = vec3(vUv.y);
        gl_FragColor = vec4(color, 1.);
    }

`;
