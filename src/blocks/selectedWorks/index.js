import css from "./styles.scss";
import { getComponents, lerp } from "../../tools/index.js"
import { COMPONENTS } from "../../components/index.js"
import anime from 'animejs/lib/anime.min.js'
import { CONFIG } from '../../config/index.js'
import * as THREE from 'three';
import * as opentype from 'opentype.js'
import {
  vertexShader,
  fragmentShader,
  projectUniforms,
  projectVertex
} from "./Text.js";
import Engine3D from '../../components/engine3D/'
import SimpleParagraph from "../../components/simple_paragraph";

const font2 = require("./Oswald_Bold.json");

export default class SelectedWorks extends React.Component {
    constructor(props) {
        super(props)
    
        this.canvas = React.createRef()
    }
      
  render() {
    let components = getComponents(this.props.data, COMPONENTS)
    return (
      <section className={css.selectedWorks} id="selectedWorks">
        <div className={css.insideSelectedWorks}>
          {components}
          <SimpleParagraph data="Sint nisi anim non eiusmod et occaecat ullamco sunt mollit. Sit qui exercitation voluptate excepteur adipisicing mollit in ipsum exercitation incididunt nisi eu. Pariatur ut in exercitation officia consectetur." />
        {/* <p className={css.seeMore}>See more works</p> */}
  
        <section ref={this.canvas} className={css.canvas}>
          <Engine3D />
        </section>
        </div>
          
      </section>
    );
  }
}