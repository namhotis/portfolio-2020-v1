import css from './styles.scss'
import CTA from '../cta/'

class Work extends React.Component {
    render() {
        let data = this.props.data[0]
        return (
            <article className={css.work} data-seed={data.disposition}>
                <a className={css.containerImage} href={data.cta.url}>
                    <img src={data.image.url} alt="" />
                    <div className={css.filter}>
                    </div>
                </a>
                <div className={css.textZone}>
                    <h2>{data.title}</h2>
                    <div dangerouslySetInnerHTML={{__html: data.text}} />
                    <CTA data={data.cta} />
                </div>
            </article>
        )
    }
}

export default Work