import css from './styles.scss'

class cursor extends React.Component {
    constructor(props) {
        super(props)

        this.cursor = React.createRef()

        this.moveCursor = this.moveCursor.bind(this)
        this.clicCursor = this.clicCursor.bind(this)
        this.upCursor = this.upCursor.bind(this)
    }

    componentDidMount() {
        window.addEventListener("mousemove", this.moveCursor)
        window.addEventListener("mousedown", this.clicCursor)
        window.addEventListener("mouseup", this.upCursor)
    }

    moveCursor(e) {
        if (this.cursor && this.cursor.current) {
            this.cursor.current.style.left = e.pageX + "px"
            this.cursor.current.style.top = e.pageY + "px"
        }
    }

    clicCursor(e) {
        if (this.cursor && this.cursor.current) {
            this.cursor.current.style.transform = "scale(2)"
            this.cursor.current.style.backgroundColor = "#eb212f"
        }
    }

    upCursor(e) {
        if (this.cursor && this.cursor.current) {
            this.cursor.current.style.transform = "scale(1)";
            this.cursor.current.style.backgroundColor = "#f2f0f0";
        }
    }

    render() {
        return <div className={["cursor", css.cursor].join(' ')} ref={this.cursor}><div className={["cursorExternal", css.cursorExternal].join(' ')}></div><div data-text="HOLD" className={["active", css.hold, css.active].join(' ')}>HOLD</div></div>
    }
}

export default cursor