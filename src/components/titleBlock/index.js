import css from './styles.scss'
import FX1 from '../fx1/'
import anime from 'animejs/lib/anime.min.js';
import { CONFIG } from '../../config/index.js'

class TitleBlock extends React.Component {
    constructor(props) {
        super(props)
        this.state = ({
            classNames: [css.titleBlock, css.left].join(' ')
        })

        this.bloc_titre = React.createRef()
    }

    componentDidMount() {
        // Set component with style "right" or "left"
        if (this.props.data[0].right_or_left == false) {
            this.setState({
                classNames:  [css.titleBlock, css.left].join(' ')
            })
        } else {
            this.setState({
                classNames: [css.titleBlock, css.right].join(' ')
            })
        }

        // Anime component when appear on-screen

        if('IntersectionObserver' in window){
          const intersectionObserver = new IntersectionObserver(
              (entries, observer) => {
                entries.forEach(entry => {
                  if (entry.isIntersecting) {
                    // Element vu
                    var tl = anime.timeline({
                      easing: "easeOutQuad",
                      duration: 900
                    });
      
                    // Add children
                    tl.add({
                      targets: entry.target.querySelector(".subtitle"),
                      // translateX: [300, 0],
                      opacity: 1,
                      filter: "blur(0px)",
                      duration: CONFIG.durationAnimation
                    })
                    tl.add({
                      targets: entry.target.querySelector(".title"),
                      // translateX: [300, 0],
                      opacity: 1,
                      filter: "blur(0px)",
                      duration: CONFIG.durationAnimation
                    }, `-=${CONFIG.durationAnimation/2}`)

                    tl.add({
                      targets: entry.target.querySelector(".second_subtitle"),
                      // translateX: [300, 0],
                      opacity: 1,
                      filter: "blur(0px)",
                      duration: CONFIG.durationAnimation
                    }, `-=${CONFIG.durationAnimation/2}`)
                    observer.unobserve(entry.target);
                  }
                });
              }
            );
    
            const elements = [this.bloc_titre.current];
            elements.forEach(element => intersectionObserver.observe(element));
          } else {
            document.querySelectorAll(".subtitle, .title, .second_subtitle").forEach(el => {
              el.style.filter = "unset"
            })
          }
    }

    render() {
        let data = this.props.data[0]
        return (
            <div ref={this.bloc_titre} className={["bloc_titre", this.state.classNames].join(' ')}>
                <p className={["subtitle", css.subtitle].join(' ')}>{data.subtitle}</p>
                <h1 className="title">{data.title}</h1>
                <p className={["second_subtitle", css.subtitle].join(' ')}>{data.second_subtitle}</p>
                <FX1 side="left" />
            </div>
        )
    }
}

export default TitleBlock