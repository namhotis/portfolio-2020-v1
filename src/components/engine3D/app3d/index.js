import MeshCustom from '../meshCustom/'
import { WebGLRenderer, Scene, PerspectiveCamera, Raycaster, Vector2 } from 'three'
const OrbitControls = require('three-orbitcontrols')
import engin3Dcss from '../styles.scss'
import { CONFIG } from '../../../config'
import cursorCSS from "../../cursor/styles.scss"

const DISABLE = false
const ENABLE = true
const QUARTER_TURN = Math.PI/2

const RED = 0x1b1c1c

export default class App3d {
    constructor(canvas, isDesktop) {
        this.windowWidth = window.innerWidth 
        this.windowHeight = window.innerHeight

        this._canvas = canvas
        this._renderer = new WebGLRenderer({ canvas, antialias: true })

        this.isDesktop = isDesktop

        this._renderer.setClearColor(RED, 1)

        this._scene = new Scene()
        this._camera = new PerspectiveCamera( 75, this.windowWidth/(this.windowHeight*.8), 0.1, 1000 )
        this._renderer.setSize( this.windowWidth, (this.windowHeight*.8) )
    
        this._raycaster = new Raycaster()
        this._mouse = new Vector2()

        this.cube = new MeshCustom("cube")
        this._scene.add(this.cube)

        this.initialCubeRotationX = 0
        this.initialCubeRotationY = 0

        this.actualDotActive = 0
        this.maxDots = document.querySelectorAll(`.${engin3Dcss.dots} li`).length
        this.worksTitle = ["Université Paris Dauphine - PSL","CNIEL - France, Terre de Lait", "Caisse d'épargne - Izicarte", "Samsung - Do what you can't"]
        this.linksWorks = ["universite-paris-dauphine", "france-terre-de-lait", "caisse-epargne-izicarte", "samsung-do-what-you-cant"]
        this.currentWorkLink = this.worksTitle[0]
        this.currentWorkID = 0

        this.animation

        // Touch events
        this.touchStart = 0
        this.touchEnd = 0

        this.isDesktop == true ? this._camera.position.z = 12 : this._camera.position.z = 18
    
        if (window.innerWidth > 767) {
            this.controls = new OrbitControls( this._camera, this._renderer.domElement )
            this.controls.enableZoom = DISABLE
        }

        this.pressureTime = 0
        this.cursorIsDown = false

        // Resier
        this.resize = this.resize.bind(this)
        this.onMouseMove = this.onMouseMove.bind(this)
        this.onMouseClic = this.onMouseClic.bind(this)
        this.goToSlide = this.goToSlide.bind(this)
        this.rotateCube = this.rotateCube.bind(this)
        this.changeProjetTitle = this.changeProjetTitle.bind(this)
        this.removeRedDot = this.removeRedDot.bind(this)
        this.addRedDot = this.addRedDot.bind(this)
        this.onMouseUp = this.onMouseUp.bind(this)
        this.holdCursor = this.holdCursor.bind(this)
        this.launchCamera = this.launchCamera.bind(this)
        this.cursorHoldShow = this.cursorHoldShow.bind(this)
        this.cursorHideShow = this.cursorHideShow.bind(this)
        this.onTouchMove = this.onTouchMove.bind(this)
        this.onTouchStart = this.onTouchStart.bind(this)
        this.onTouchEnd = this.onTouchEnd.bind(this)
        this.rotateCubePhone = this.rotateCubePhone.bind(this)
        this.rotateCubePhoneCounterclock = this.rotateCubePhoneCounterclock.bind(this)

        this.isMobile = false;
        if ((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1)) {
            this.isMobile = true;
        }

        if (this.isMobile == false) {
            window.addEventListener( 'mousemove', this.onMouseMove, false )
            document.querySelector(`.${engin3Dcss.canvas}`).addEventListener( 'mouseover', this.cursorHoldShow, false )
            document.querySelector(`.${engin3Dcss.canvas}`).addEventListener( 'mouseout', this.cursorHideShow, false )
            document.querySelector(`.${engin3Dcss.canvas}`).addEventListener( 'mousedown', this.onMouseClic, false )
            document.querySelector(`.${engin3Dcss.canvas}`).addEventListener( 'mouseup', this.onMouseUp, false )
            document.querySelectorAll(`.${engin3Dcss.dots} li`).forEach((e, index) => { e.querySelector("div").addEventListener("mousedown", this.goToSlide(index)) })
        }
        
        window.addEventListener('resize', this.resize)
        window.addEventListener( 'touchmove', this.onTouchMove, false )
        document.querySelector(`.${engin3Dcss.canvas}`).addEventListener( 'touchstart', this.onTouchStart, false )
        document.querySelector(`.${engin3Dcss.canvas}`).addEventListener( 'touchend', this.onTouchEnd, false )
    }

    resize() {
        this._camera.aspect = this.windowWidth / (this.windowHeight*.8)
        this._camera.updateProjectionMatrix()
        this._renderer.setSize( this.windowWidth, (this.windowHeight*.8) )
    }

    dispose() {
        window.removeEventListener('resize', this.resize)
        document.querySelector(`.${engin3Dcss.canvas}`).removeEventListener('touchstart', this.onTouchStart, false) 
        document.querySelector(`.${engin3Dcss.canvas}`).removeEventListener('touchend', this.onTouchEnd, false) 
        window.removeEventListener( 'touchmove', this.onTouchMove, false )
        
        if (this.isMobile == false) {
            window.removeEventListener( 'mousemove', this.onMouseMove, false )
            document.querySelector(`.${engin3Dcss.canvas}`).removeEventListener( 'mouseover', this.cursorHoldShow, false )
            document.querySelector(`.${engin3Dcss.canvas}`).removeEventListener( 'mouseout', this.cursorHideShow, false )
            document.querySelector(`.${engin3Dcss.canvas}`).removeEventListener( 'mousedown', this.onMouseClic, false )
            document.querySelector(`.${engin3Dcss.canvas}`).removeEventListener( 'mouseup', this.onMouseUp, false )
            document.querySelectorAll(`.${engin3Dcss.dots} li`).forEach((e, index) => { e.querySelector("div").removeEventListener("mousedown", this.goToSlide(index)) })
        }
    }

    onTouchStart(e) {
        this.touchStart = e.touches[0].clientX
    }

    onTouchEnd(e) {
        if (this.touchStart > this.touchEnd) {
            this.difference = this.touchStart - this.touchEnd
            if (this.difference >= 50) {
                this.cube.rotation.y = this.initialCubeRotationY 
                this.removeRedDot(this.actualDotActive)
                this.rotateCubePhoneCounterclock()
                this.actualDotActive <= 0 ? this.actualDotActive = 3 : this.actualDotActive -= 1
                document.querySelector(".cursorExternal").classList.remove(cursorCSS.active)
                this.currentWorkID = this.actualDotActive
                this.addRedDot(this.actualDotActive)
                this.changeProjetTitle(this.actualDotActive)
            }
        } else if (this.touchStart < this.touchEnd) {
            this.difference = this.touchEnd - this.touchStart 
            if (this.difference >= 50) {
                this.cube.rotation.y = this.initialCubeRotationY 
                this.rotateCubePhone()
                this.removeRedDot(this.actualDotActive)
                this.actualDotActive < this.maxDots-1 ? this.actualDotActive += 1 : this.actualDotActive = 0
                this.currentWorkID = this.actualDotActive
                this.addRedDot(this.actualDotActive)
                this.changeProjetTitle(this.actualDotActive)
                document.querySelector(".cursorExternal").classList.remove(cursorCSS.active)
            }
        }
    }

    cursorHoldShow() {
        document.querySelector(".cursorExternal").style.display = "initial"
        document.querySelector(".active").style.display = "initial"
    }

    cursorHideShow() {
        document.querySelector(".cursorExternal").style.display = "none"
        document.querySelector(".active").style.display = "none"
    }

    onMouseMove(e) {
        this._mouse.x = ( e.clientX / this.windowWidth ) * 2 - 1
        this._mouse.y = - ( e.clientY / this.windowHeight ) * 2 + 1

        this.cube.rotation.x = this.initialCubeRotationX - this._mouse.y/3
        this.cube.rotation.y = this.initialCubeRotationY - this._mouse.x*1
    }

    onTouchMove(e) {
        this.touchEnd = e.touches[0].clientX
    }

    onMouseUp(e) {
        this.cursorIsDown = false
        this.currentWorkID += 1

        if (this.pressureTime < 120) {
            this.cube.rotation.y = this.initialCubeRotationY - this._mouse.x*1
            this.rotateCube()
            this.removeRedDot(this.actualDotActive)
            this.actualDotActive < this.maxDots-1 ? this.actualDotActive += 1 : this.actualDotActive = 0
            this.currentWorkID = this.actualDotActive
            this.addRedDot(this.actualDotActive)
            this.changeProjetTitle(this.actualDotActive)
            document.querySelector(".cursorExternal").classList.remove(cursorCSS.active)
        }

        this.pressureTime = 0
        cancelAnimationFrame(this.holdAnimationCursor)
    }

    onMouseClic(e) {
        e.preventDefault()
        this.cursorIsDown = true
        this.holdCursor()
    }

    holdCursor() {
        this.holdAnimationCursor = requestAnimationFrame(this.holdCursor)
        this.pressureTime += 1
        document.querySelector(".cursorExternal").classList.add(cursorCSS.active)

        if (this.pressureTime >= CONFIG.holdTime) {
            this.launchCamera()
            document.querySelector(`.${engin3Dcss.canvas}`).removeEventListener( 'mouseup', this.onMouseUp, false )
            setTimeout(() => {
                window.location.href = "/works/" + this.linksWorks[this.currentWorkID]
            }, 500)
        }
    }

    rotateCube() {
        this.cube.rotation.y += Math.PI/44
        if (this.cube.rotation.y < this.initialCubeRotationY - this._mouse.x/1.5 + QUARTER_TURN) {
            requestAnimationFrame(this.rotateCube)
            window.removeEventListener( 'mousemove', this.onMouseMove, false )
        } else {
            cancelAnimationFrame(this.rotateCube)
            window.addEventListener( 'mousemove', this.onMouseMove, false )
            this.initialCubeRotationY += QUARTER_TURN
        }
    }

    rotateCubePhone() {
        this.cube.rotation.y += Math.PI/44
        if (this.cube.rotation.y < this.initialCubeRotationY + QUARTER_TURN) {
            requestAnimationFrame(this.rotateCubePhone)
        } else {
            cancelAnimationFrame(this.rotateCubePhone)
            this.cube.rotation.y = this.initialCubeRotationY + QUARTER_TURN
            this.initialCubeRotationY += QUARTER_TURN
        }
    }

    rotateCubePhoneCounterclock() {
        this.cube.rotation.y -= Math.PI/44
        if (this.cube.rotation.y > this.initialCubeRotationY - QUARTER_TURN) {
            requestAnimationFrame(this.rotateCubePhoneCounterclock)
        } else {
            cancelAnimationFrame(this.rotateCubePhoneCounterclock)
            this.cube.rotation.y = this.initialCubeRotationY - QUARTER_TURN
            this.initialCubeRotationY -= QUARTER_TURN
        }
    }

    goToSlide = (PROJECT_NUMBER) => e => {
        this.cube.rotation.y = PROJECT_NUMBER*QUARTER_TURN - this._mouse.x/1.5
        this.initialCubeRotationY = PROJECT_NUMBER*QUARTER_TURN
        this.changeProjetTitle(PROJECT_NUMBER)
        this.removeRedDot(this.actualDotActive)
        this.actualDotActive = PROJECT_NUMBER
        this.currentWorkID = PROJECT_NUMBER
        this.addRedDot(PROJECT_NUMBER)
    }

    removeRedDot(PROJECT_NUMBER) {
        document.querySelectorAll(`.${engin3Dcss.dots} li`)[PROJECT_NUMBER].querySelector("div").classList.remove(engin3Dcss.active)
    }

    addRedDot(PROJECT_NUMBER) {
        document.querySelectorAll(`.${engin3Dcss.dots} li`)[PROJECT_NUMBER].querySelector("div").classList.add(engin3Dcss.active)
    }

    changeProjetTitle(PROJECT_NUMBER) {
        document.querySelector(`.${engin3Dcss.projectName}`).innerHTML = this.worksTitle[PROJECT_NUMBER]
    }

    launchCamera() {
        requestAnimationFrame(this.launchCamera)
        this._camera.position.z += 0.1
    }

    render() {
        if (this.controls) {
            this.controls.update()
        }
        this._renderer.render(this._scene, this._camera)
    }
}