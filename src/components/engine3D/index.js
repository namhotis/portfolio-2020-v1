import { Component, createRef } from 'react'
import App3d from './app3d/index.js'
import css from './styles.scss'

const BIGWINDOW = true
const SMALLWINDOW = false
export default class Engine3D extends Component {
    constructor(props) {
        super(props)

        this._canvas = createRef()
        this.dots = createRef()

        this.state = { isDesktop: true }

        this.createApp3DWithCameraPlace = this.createApp3DWithCameraPlace.bind(this)
    }

    componentDidMount() {
        if (window.outerWidth < 768) {
            this.setState({
                isDesktop: false
            }, this.createApp3DWithCameraPlace(SMALLWINDOW))
        } else {
            this.createApp3DWithCameraPlace(BIGWINDOW)
        }

        const update = () => {
            this._request = window.requestAnimationFrame(update)
            this._app3D.render()
        }

        this._request = window.requestAnimationFrame(update)

        this.redDots = this.dots.current.querySelectorAll('li div')
        this.redDot = this.redDots[0]
        this.redDot.classList.add(css.active)
    }

    createApp3DWithCameraPlace(sizeWindow) {
        this._app3D = new App3d(this._canvas.current, sizeWindow)
    }

    componentWillUnmount() {
        window.cancelAnimationFrame(this._request)
    }

    render() {
        return (
            <>
                <div className={css.canvas}>
                    <canvas ref={this._canvas}/>
                </div>
                <h2 className={css.projectName}>Université Paris Dauphine - PSL</h2>
                <ul ref={this.dots} className={css.dots}>
                    <li><div></div></li>
                    <li><div></div></li>
                    <li><div></div></li>
                    <li><div></div></li>
                </ul>
            </>
        )
    }
}