// import {
//     BoxGeometry,
//     Mesh,
//     MeshBasicMaterial,
//     TextureLoader,
//     MeshFaceMaterial,
//     DoubleSide
// } from 'three'

// const MESHES = {
//     cube: BoxGeometry
// }
// // MESH['a'] = CubeGeometry

// export default class MeshCustom {
//     constructor(value) {
//         const geo = MeshCustom.createLetterGeo(value)
        
//         var textureLoader = new TextureLoader();

//         var texture0 = textureLoader.load( '/images/paris_dauphine.jpg' );
//         var texture1 = textureLoader.load( '/images/ftdl.jpg' );
//         var texture2 = textureLoader.load( '/images/samsungNoel2018.webp' );
//         var texture3 = textureLoader.load( '/images/caisse-depargne-logo.jpg' );

//         var materials = [
//             new MeshBasicMaterial( { map: texture2, side: DoubleSide } ),
//             new MeshBasicMaterial( { map: texture1, side: DoubleSide } ),
//             new MeshBasicMaterial( {color: 0x100f0f, side: DoubleSide} ),
//             new MeshBasicMaterial( {color: 0x100f0f, side: DoubleSide} ),
//             new MeshBasicMaterial( { map: texture0, side: DoubleSide } ),
//             new MeshBasicMaterial( { map: texture3, side: DoubleSide  } )
//         ];
//         var mat = new MeshFaceMaterial( materials );

//         this.cube = new Mesh(geo, mat);

//         return this.cube
//     }

//     static createLetterGeo(value) {
//         return new BoxGeometry(5, 3, 5)
//         // return new MESHES[value](1, 1, 1)
//     }
// }

import {
    BoxGeometry,
    Mesh,
    MeshBasicMaterial,
    TextureLoader,
    MeshFaceMaterial,
    DoubleSide,
    PlaneGeometry,
    Object3D,
    LinearFilter
} from 'three'

const MESHES = {
    cube: BoxGeometry,
    plane: PlaneGeometry
}
// MESH['a'] = CubeGeometry

export default class MeshCustom {
    constructor(value) {
        const geo = MeshCustom.createLetterGeo("plane")
        
        var textureLoader = new TextureLoader();
        
        var texture0 = textureLoader.load( '/images/paris_dauphine.jpg' );
        var texture1 = textureLoader.load( '/images/ftdl.jpg' );
        var texture2 = textureLoader.load( '/images/caisse-depargne-logo.jpg' );
        var texture3 = textureLoader.load( '/images/samsungNoel2018.webp' );
        
        var materials = [
            new MeshBasicMaterial( { map: texture0, side: DoubleSide } ),
            new MeshBasicMaterial( { map: texture1, side: DoubleSide } ),
            new MeshBasicMaterial( { map: texture2, side: DoubleSide } ),
            new MeshBasicMaterial( { map: texture3, side: DoubleSide  } )
        ];

        this.face0 = new Mesh(geo, materials[0]);
        this.face1 = new Mesh(geo, materials[1]);
        this.face2 = new Mesh(geo, materials[2]);
        this.face3 = new Mesh(geo, materials[3]);
        
        this.cube = new Object3D();
        this.cube.add(this.face0)
        this.face0.rotation.y = 0*Math.PI/2
        this.face0.position.z = 7
        this.cube.add(this.face1)
        this.face1.rotation.y = 3*Math.PI/2
        this.face1.position.x = -7
        this.cube.add(this.face2)
        this.face2.rotation.y = 2*Math.PI/2
        this.face2.position.z = -7
        this.cube.add(this.face3)
        this.face3.rotation.y = 1*Math.PI/2
        this.face3.position.x = 7
        
        

        return this.cube
    }

    static createLetterGeo(value) {
        // return new BoxGeometry(5, 3, 5)
        return new MESHES[value](7, 4, 0.01)
    }
}