import css from './styles.scss'

const FX1 = props => {
    if (props.side == "left") {
        return <div className={[css.fx1, css.left].join(' ')}></div>
    } else {
        return <div className={[css.fx1, css.right].join(' ')}></div>
    } 
    
}

export default FX1