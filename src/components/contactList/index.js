import css from './styles.scss'

const ContactList = props => {
    return (
        <ul className={css.contactList}>
            <li><h2>Mathis DYK</h2></li>
            <li>Paris 11</li>
            <li><a href="tel:06.01.45.58.78">06.01.45.58.78</a></li>
            <li><a href="mailto:mathis.dyk.pro@gmail.com">mathis.dyk.pro@gmail.com</a></li>
        </ul>
    )
}

export default ContactList