import css from './styles.scss'
import { CONFIG } from '../../config/index.js'
import anime from 'animejs/lib/anime.min.js';

class SimpleParagraph extends React.Component {
    constructor(props) {
        super(props)

        this.paragraph = React.createRef()
    }

    componentDidMount() {
      // if(!!window.IntersectionObserver){}
      // /* or */
      if('IntersectionObserver' in window){
        const intersectionObserver = new IntersectionObserver(
          (entries, observer) => {
            entries.forEach(entry => {
              if (entry.isIntersecting) {
                // Element vu
                var tl = anime.timeline({
                  easing: "easeOutQuad",
                  delay: 100
                });
                tl.add({
                  targets: entry.target,
                  // translateX: [300, 0],
                  opacity: 1,
                  filter: "blur(0px)",
                  duration: CONFIG.durationAnimation
                })
                observer.unobserve(entry.target);
              }
            });
          }
        );
  
        const elements = [this.paragraph.current];
  
        elements.forEach(element => intersectionObserver.observe(element));
      }
    }

    render() {
        return <div ref={this.paragraph} className={["paragraph", css.paragraph].join(' ')} dangerouslySetInnerHTML={{__html: this.props.data}}></div>
    }
}

export default SimpleParagraph