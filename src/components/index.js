import Button from './button'
import CTA from './cta'
import FX1 from './fx1'
import Header from './header'
import SideText from './sideText'
import SimpleParagraph from './simple_paragraph'
import TitleBlock from './titleBlock'
import Work from './work'
import ContactList from './contactList'

export const COMPONENTS = {
    button : Button,
    cta : CTA,
    fx1 : FX1,
    header : Header,
    side_vertical_text : SideText,
    paragraph : SimpleParagraph,
    bloc_titre : TitleBlock,
    work_trailer : Work,
    contact_list : ContactList
}