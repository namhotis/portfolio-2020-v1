import css from './styles.scss'

const CTA = props => {
    return <a href={props.data.url}><button className={css.red_cta}>{props.data.title}</button></a>
}

export default CTA