import css from './styles.scss'

const SideText = (props) => {
    let data = props.data[0]
    if (data.right_or_left == false) {
        return <div className={[css.SideText, css.left].join(' ')}><div className={css.whiteBar}></div><p>{data.text}</p><div className={css.whiteBar}></div></div>
    } else {
        return <div className={[css.SideText, css.right].join(' ')}><div className={css.whiteBar}></div><p>{data.text}</p><div className={css.whiteBar}></div></div>
    }
}

export default SideText