import css from './styles.scss'
import Link from "next/link"

const Nav = props => {
    return (
        props.data.map(link => (
            <li key={link.link.title}>
                <Link href={link.link.url}>
                    <a>
                        {link.link.title}
                    </a>
                </Link>
            </li>
        ))
    )
}

export default class Header extends React.Component {
    constructor(props) {
        super(props)

        this.header = React.createRef()
        this.nav = React.createRef()
    }

    componentDidMount() {
        this.prev = window.scrollY;
        window.addEventListener('scroll', e => this.handleNavigation(e));
    }
    
    handleNavigation = (e) => {
        const window = e.currentTarget;
    
        if ((this.prev > window.scrollY) && this.nav && this.nav.current) {
            this.nav.current.style.top = "0px"
            this.nav.current.style.opacity = "1"
        } else if ((this.prev < window.scrollY) && this.nav && this.nav.current) {
            this.nav.current.style.top = "-110px"
            this.nav.current.style.opacity = "0"
        }
        this.prev = window.scrollY;
    };

    render() {
        return (
            <header ref={this.header}>
                <Link href="/#homepageHeader">
                    <a>
                        <img
                        src={this.props.data.logo}
                        alt="logo de Mathis DYK"
                        className={css.mainLogo}
                        />
                    </a>
                </Link>
    
                <nav className={css.nav} ref={this.nav}>
                    <ul>
                        <Nav data={this.props.data.navigation} />
                    </ul>
                </nav>
            </header>
        )
    }
}