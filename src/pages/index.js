import css from "./styles.scss"
import { getBlockWithType } from '../blocks/index.js'
import "../config/styles.scss"
import Header from '../components/header/index.js'
import { callAPI } from '../tools/index.js'
import Link from 'next/link'
import Cursor from '../components/cursor/index.js'
import SelectedWorks from '../blocks/selectedWorks/'

export default class Index extends React.Component {
  constructor(props) {
    super(props)
  }

  static async getInitialProps(ctx) {
    const data = await callAPI(ctx.pathname)
    return { data }
  }

  render() {
    const data = this.props.data[0].acf
    const blocksData = data.blocks

    const blocks = blocksData.map((block, index) => {
        let Block = getBlockWithType(block.block_type)
        return <Block data={block[block.block_type]} key={index}/>
    })

  return (
  <>
    <div className={css.noise}>
      <Header data={data} /> 
      {blocks}
      <Cursor />

      {/* <SelectedWorks /> */}
    </div>
  </>
  )}
}